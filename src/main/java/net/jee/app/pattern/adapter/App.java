package net.jee.app.pattern.adapter;

/**
 * Created by Administrator on 2015/11/5.
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Adapter Pattern.");

        Target target = new Target();
        target.request();

        target = new Adapter(new Adaptee());
        target.request();

    }
}
