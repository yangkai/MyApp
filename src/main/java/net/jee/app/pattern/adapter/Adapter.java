package net.jee.app.pattern.adapter;

/**
 * Created by Administrator on 2015/11/5.
 */
public class Adapter extends Target {
    private Adaptee adaptee;

    public Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public void request() {
        System.out.println("适配器处理请求是这样的...");
        super.request();
        this.adaptee.doRequest();
    }
}
