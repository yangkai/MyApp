package net.jee.app.pattern.bridge.corp;

import net.jee.app.pattern.bridge.Corp;
import net.jee.app.pattern.bridge.Product;

public class BookCorp extends Corp {
	
	public BookCorp(Product product) {
		super(product);
	}

	@Override
	public void makeMoney() {
		super.makeMoney();
		System.out.println("书店开始挣钱了");
	}

}
