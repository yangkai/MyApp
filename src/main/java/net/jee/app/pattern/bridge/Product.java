package net.jee.app.pattern.bridge;

public interface Product {
	
	void beProduct();
	
	void beSell();

}
