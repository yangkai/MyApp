package net.jee.app.pattern.bridge;

import net.jee.app.pattern.bridge.corp.BookCorp;
import net.jee.app.pattern.bridge.corp.ShanzhaiCo;
import net.jee.app.pattern.bridge.product.Book;
import net.jee.app.pattern.bridge.product.Clothes;
import net.jee.app.pattern.bridge.product.Phone;



public class App {

	
	public static void main(String[] args) {
		System.out.println("------山寨衣服公司------");
		Corp corp = new BookCorp(new Book());
		corp.makeMoney();
		
		System.out.println("------山寨手机公司------");
		Corp shanzhaiCo = new ShanzhaiCo(new Phone());
		shanzhaiCo.makeMoney();
		
		System.out.println("------山寨衣服公司------");
		shanzhaiCo = new ShanzhaiCo(new Clothes());
		shanzhaiCo.makeMoney();
	}
	
}
