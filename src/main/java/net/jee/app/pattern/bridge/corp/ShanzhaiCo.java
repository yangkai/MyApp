package net.jee.app.pattern.bridge.corp;

import net.jee.app.pattern.bridge.Corp;
import net.jee.app.pattern.bridge.Product;

public class ShanzhaiCo extends Corp {


	public ShanzhaiCo(Product product) {
		super(product);
	}

	@Override
	public void makeMoney() {
		super.makeMoney();
		System.out.println("山寨公司暴利");
		
	}

}
