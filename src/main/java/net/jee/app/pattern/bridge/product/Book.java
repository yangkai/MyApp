package net.jee.app.pattern.bridge.product;

import net.jee.app.pattern.bridge.Product;


public class Book implements Product {

	@Override
	public void beProduct() {
		// TODO Auto-generated method stub
		System.out.println("印刷书本");
	}

	@Override
	public void beSell() {
		// TODO Auto-generated method stub
		System.out.println("书店卖书");
	}

}
