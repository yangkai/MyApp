package net.jee.app.pattern.bridge.product;

import net.jee.app.pattern.bridge.Product;


public class Clothes implements Product {

	@Override
	public void beProduct() {
		System.out.println("生产衣服");
	}

	@Override
	public void beSell() {
		System.out.println("销售衣服");
	}

}
