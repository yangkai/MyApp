package net.jee.app.pattern.bridge.product;

import net.jee.app.pattern.bridge.Product;

public class Phone implements Product {

	@Override
	public void beProduct() {
		// TODO Auto-generated method stub
		System.out.println("生产手机");
	}

	@Override
	public void beSell() {
		// TODO Auto-generated method stub
		System.out.println("销售手机");
	}

}
