package net.jee.app.pattern.bridge;



public abstract class Corp {
	
	Product product;
	
	public Corp(Product product) {
		super();
		this.product = product;
	}

	public void makeMoney() {
		this.product.beProduct();
		this.product.beSell();
	}

}
