package net.jee.app.pattern.decorator;

/**
 * Created by Administrator on 2015/11/5.
 */
public class Decorator implements Man {

    private Man man;

    public Decorator(Man man) {
        this.man = man;
    }


    @Override
    public void walk() {
        this.man.walk();
    }
}
