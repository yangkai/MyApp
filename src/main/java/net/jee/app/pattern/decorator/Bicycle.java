package net.jee.app.pattern.decorator;

/**
 * Created by Administrator on 2015/11/5.
 */
public class Bicycle extends Decorator {

    public Bicycle(Man man) {
        super(man);
    }

    @Override
    public void walk() {
        System.out.println("采用自行车代步");
        super.walk();
    }
}
