package net.jee.app.pattern.decorator;

/**
 * Created by Administrator on 2015/11/5.
 */
public class Motorbike extends Decorator {
    public Motorbike(Man man) {
        super(man);
    }

    @Override
    public void walk() {
        System.out.println("用摩托车代步");
        super.walk();
    }
}
